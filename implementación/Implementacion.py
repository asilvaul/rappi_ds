#!/usr/bin/env python
# coding: utf-8

# In[85]:


import pickle as pickle
import numpy as np
import pandas as pd


# In[86]:


base2score=pd.read_csv('input/base2score202111.csv')


# In[87]:


#Acotamiento de variables
COTAS=pd.read_csv("essentials/COTAS.csv",sep=',', encoding ='latin1')
def acotamiento(dfr):
    for i in range(len(COTAS)):
        VAR   = COTAS.loc[i,'VARIABLE']
        PER5  = COTAS.loc[i,'PER5']
        PER95 = COTAS.loc[i,'PER95']
        dfr[VAR+'_acot'] = dfr[VAR].apply(lambda x: PER5 if x < PER5 else 
                                        PER95 if x > PER95 else 
                                        x)                               
    return dfr
    
base2score_1 = acotamiento(base2score)


# In[88]:


# Transformacion
num=['dia_acot_A2','horas_slo_acot_A1','ctd_operbyuser_acot_A1','linea_tc_acot_A1']
cat=['genero_st']


# In[89]:


EQUATION_A1=pd.read_csv("essentials/EQUATION_A1.csv",sep=',', encoding ='latin1')
EQUATION_A2=pd.read_csv("essentials/EQUATION_A2.csv",sep=',', encoding ='latin1')


# In[90]:


def objective_A1(x, a, b):
    return a * x + b
def objective_A2(x, a, b, c):
    return a * (x**2) + b*x +c


# In[91]:


for i in range(len(EQUATION_A1)):
    base2score_1[str(EQUATION_A1.iloc[i,0])+'_A1']=objective_A1(base2score_1[EQUATION_A1.iloc[i,0]],
                                                                       EQUATION_A1.iloc[i,1],
                                                                       EQUATION_A1.iloc[i,2])
for i in range(len(EQUATION_A2)):
    base2score_1[str(EQUATION_A2.iloc[i,0])+'_A2']=objective_A2(base2score_1[EQUATION_A2.iloc[i,0]],
                                                                       EQUATION_A2.iloc[i,1],
                                                                       EQUATION_A2.iloc[i,2],
                                                                       EQUATION_A2.iloc[i,3])
base2score_1.loc[base2score_1['genero']=='F','genero_st']=-0.8028964989252048
base2score_1.loc[base2score_1['genero']=='M','genero_st']=-0.9813220337835374


# In[92]:


# Missing
MISSING_VALUESCAT=pd.read_csv("essentials/missing_valuescat.csv",sep=',', encoding ='latin1')
MISSING_VALUES=pd.read_csv("essentials/missing_values.csv",sep=',', encoding ='latin1')


# In[93]:


for i in range(len(cat)):
    base2score_1.loc[base2score_1[cat[i]].isna()==True,cat[i]]=MISSING_VALUESCAT['MISSING_VAL'][i]
    
for i in range(len(num)):
    base2score_1.loc[base2score_1[num[i]].isna()==True,num[i]]=MISSING_VALUES['MISSING_VAL'][i]


# In[94]:


#Leer el modelo
path_modelo = 'essentials/Fraudster_dtv1.pickle'
MODELO=pickle.load(open(path_modelo,'rb'))


# In[95]:


base2score_1['r_monto_linea_acot_A2']=0
base2score_1['ciudad_st']=0
base2score_1['ctd_oper0_acot_A1']=0
base2score_1['montoant_acot_A1']=0
base2score_1['interes_tc_acot_A1']=0
base2score_1['var_monto_acot_A1']=0


# In[96]:


var_finales=['r_monto_linea_acot_A2',
 'ctd_operbyuser_acot_A1',
 'ciudad_st',
 'genero_st',
 'ctd_oper0_acot_A1',
 'montoant_acot_A1',
 'linea_tc_acot_A1',
 'interes_tc_acot_A1',
 'horas_slo_acot_A1',
 'dia_acot_A2',
 'var_monto_acot_A1']


# In[97]:


#scorear la base


# In[98]:


probs = MODELO.predict_proba(base2score_1[var_finales])[:,1]
base2score_1['Pf']=probs
tresh=0.283 #Punto de corte optimo
base2score_1.loc[base2score_1['Pf']>=tresh,'PRED_TRESH']=1
base2score_1.loc[base2score_1['Pf']<tresh,'PRED_TRESH']=0


# In[99]:


base2score_1.to_csv('output/basescored.csv',index=False)

