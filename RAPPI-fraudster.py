#!/usr/bin/env python
# coding: utf-8

# In[620]:


import pickle as pickle
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sklearn
from sklearn.metrics import auc, accuracy_score, confusion_matrix, mean_squared_error
from sklearn.model_selection import cross_val_score, GridSearchCV, RandomizedSearchCV, train_test_split, KFold, cross_val_score, train_test_split
from sklearn.linear_model import ElasticNet, Lasso,  BayesianRidge, LassoLarsIC,LinearRegression, LogisticRegression
import xgboost as xgb
from sklearn.metrics import r2_score
from boruta import BorutaPy
import itertools
from datetime import datetime
import sklearn.metrics as metrics
from sklearn.tree import DecisionTreeClassifier

from scipy.optimize import curve_fit
import math
import lightgbm as lgb
import statsmodels.api as sm

from time import sleep
import ast
from sklearn.model_selection import StratifiedKFold, cross_validate
import graphviz
from sklearn import tree


# # Importar registros

# In[102]:


baseinit=pd.read_csv('ds_challenge_2021.csv')


# In[103]:


# Creacion de variable dia
baseinit['dia']=[datetime.strptime(date, '%m/%d/%Y').day for date in baseinit['fecha']]


# In[104]:


baseinit['r_monto_linea'] = baseinit['monto']/baseinit['linea_tc']
baseinit['r_monto_linea'] = baseinit['r_monto_linea'].astype(float)


# In[105]:


#Split de la variable dispositivo en device score y os
device_score=[]
os=[]
for cont in range(len(baseinit)):
    device_score.append(ast.literal_eval(baseinit['dispositivo'][cont])['device_score'])
    os.append(ast.literal_eval(baseinit['dispositivo'][cont])['os'])


# In[109]:


# Creacion de variables
montoant=[]
ctd_operbyuser=[]
horas_ant=[]
ctd_oper1=[]
ctd_oper0=[]
dif_os=[]
dif_est=[]
baseinit['diahora']=(baseinit['dia']*24)+baseinit['hora']
for cont in range(len(baseinit)):
    id_user=baseinit['ID_USER'][cont]
    diahora=baseinit['diahora'][cont]
    if len(baseinit.loc[(baseinit['ID_USER']==id_user)&
                        (baseinit['diahora']<diahora)].sort_values(by=['diahora'],
                                                                   ascending=False))==0:
        montoant.append(np.nan)
        ctd_operbyuser.append(1)
        horas_ant.append(np.nan)
        ctd_oper1.append(len(baseinit.loc[(baseinit['ID_USER']==id_user)&(baseinit['diahora']<=diahora)&
                                          (baseinit['status_txn']=='Aceptada')]))
        ctd_oper0.append(len(baseinit.loc[(baseinit['ID_USER']==id_user)&(baseinit['diahora']<=diahora)&
                                          (baseinit['status_txn']=='Rechazada')]))
        dif_os.append(1)
        dif_est.append(1)
    else:
        basex=baseinit.loc[(baseinit['ID_USER']==id_user)&
                           (baseinit['diahora']<diahora)].sort_values(by=['diahora'],ascending=False).reset_index()
        montoant.append(basex['monto'][0])
        ctd_operbyuser.append(len(basex)+1)
        horas_ant.append(basex['diahora'][0])
        ctd_oper1.append(len(baseinit.loc[(baseinit['ID_USER']==id_user)&(baseinit['diahora']<=diahora)&
                                          (baseinit['status_txn']=='Aceptada')]))
        ctd_oper0.append(len(baseinit.loc[(baseinit['ID_USER']==id_user)&(baseinit['diahora']<=diahora)&
                                          (baseinit['status_txn']=='Rechazada')]))
        dif_os.append(len(baseinit.loc[(baseinit['ID_USER']==id_user)&(baseinit['diahora']<=diahora)]['os'].unique()))
        dif_est.append(len(baseinit.loc[(baseinit['ID_USER']==id_user)&(baseinit['diahora']<=diahora)]['establecimiento'].unique()))
    print(cont)


# In[110]:


baseinit['montoant']=montoant # Monto de operacion anterior
baseinit['ctd_operbyuser']=ctd_operbyuser #Cantidad de operaciones del usuario
baseinit['horas_ant']=horas_ant # Para calculo
baseinit['ctd_oper1']=ctd_oper1 # Cantidad de operaciones aceptadas
baseinit['ctd_oper0']=ctd_oper0 # Cantidad de operaciones rechazadas
baseinit['dif_os']=dif_os # Diferentes sistemas operativos
baseinit['dif_est']=dif_est # Diferentes establecimientos


# In[127]:


baseinit['var_monto']=(baseinit['monto']/baseinit['montoant'])-1 # Variacion del monto respecto a la ultima operacion
baseinit['horas_slo']=(baseinit['diahora']-baseinit['horas_ant']) # Horas transcurridas desde la ultima operacion


# In[ ]:


# Seteamos el target como si el cliente tendra fraude en un futuro o si tuvo en algun momento


# In[128]:


listado=baseinit.loc[baseinit['fraude']==1]['ID_USER']


# In[129]:


baseinit.loc[baseinit['ID_USER'].isin(listado),'fraude2']=True
baseinit.loc[~baseinit['ID_USER'].isin(listado),'fraude2']=False


# In[130]:


base=baseinit.copy()


# ## Datos importantes de la base

# In[131]:


print(len(base))
print(base['fraude2'].sum())
print(round(base['fraude2'].sum()/len(base),5))


# In[140]:


# Dropeo de variables no importantes para el analisis
var_x=list(base.columns)
var_x_1=var_x
var_x_1.remove('ID_USER') # Id
var_x_1.remove('fraude2') # Target
var_x_1.remove('dispositivo') # Se separo
var_x_1.remove('fecha') # Se tiene el dia
var_x_1.remove('fraude') # Target foto
var_x_1.remove('horas_ant') # Ya esta incluida en horas_slo
var_x_1.remove('diahora') #Ya esta incluida


# In[141]:


# Categorizacion de variables
continua=[]
catg=[]
disc=[]
for var in var_x_1:
    print(var)
    if len(base[var].unique()) > len(base)*0.01:
        print('continua')
        continua.append(var)
    elif len(base[var].unique()) > 8:
        print('discreta')
        disc.append(var)
    else :
        print('categorica')
        catg.append(var)


# In[142]:


# Categorizacion manual
disc.append('dif_os')
disc.append('dif_est')
#disc.append('dif_ciu')

catg.remove('dif_os')
catg.remove('dif_est')
#catg.remove('dif_ciu')


# In[143]:


#Analisis de variables
for varcat in catg:
    print(varcat)
    print(base[varcat].unique())
    print("********************************************")


# # ANALISIS DE VARIABLES CONTINUAS

# In[148]:



X_train, X_test, y_train, y_test = train_test_split(base[var_x_1],base['fraude2'],
                                                    test_size=0.3,random_state=1234,stratify=base['fraude2'])
ginis_na=[]
ginis_nona=[]
max_val=[]
min_val=[]
mean_val=[]
P5=[]
P95=[]
miss=[]
for cont in continua+disc:
    #sm.Logit(y_train,X_train[cont]).fit().summary()
    #clf=sm.Logit(y_train,X_train[cont]).fit()
    #gini = 2*metrics.roc_auc_score(y_test, clf.predict(X_test[cont])) -1
    #ginis_na.append(gini*100)
    max_val.append(base[cont].max())
    min_val.append(base[cont].min())
    mean_val.append(base[cont].mean())
    P5.append(np.nanpercentile(base[cont], 5))
    P95.append(np.nanpercentile(base[cont], 95))
    miss.append((len(base.loc[base[cont].isna()==True])/len(base))*100)

for cont in continua+disc:
    newxtrain=X_train.loc[X_train[cont].isna()==False].copy()
    newytrain=y_train.drop(index=X_train.loc[X_train[cont].isna()==True].index)
    newxtest=X_test.loc[X_test[cont].isna()==False].copy()
    newytest=y_test.drop(index=X_test.loc[X_test[cont].isna()==True].index)
    #sm.Logit(newytrain,newxtrain[cont]).fit().summary()
    clf=sm.Logit(newytrain,newxtrain[cont]).fit()
    gini = 2*metrics.roc_auc_score(newytest, clf.predict(newxtest[cont])) -1
    ginis_nona.append(gini*100)


# In[149]:



ginis_na_nl=[]

for cont in continua+disc:
    #sm.Logit(y_train,X_train[cont]).fit().summary()
    newxtrain=X_train.loc[X_train[cont].isna()==False].copy()
    newytrain=y_train.drop(index=X_train.loc[X_train[cont].isna()==True].index)
    newxtest=X_test.loc[X_test[cont].isna()==False].copy()
    newytest=y_test.drop(index=X_test.loc[X_test[cont].isna()==True].index)
    
    tree = DecisionTreeClassifier(random_state = 1234)
    tree.fit(newxtrain[[cont]],newytrain)
    gini = 2*metrics.roc_auc_score(newytest, tree.predict_proba(newxtest[[cont]])[:,1]) -1
    ginis_na_nl.append(gini*100)


# In[151]:


# Principales datos de variables numericas
pd.DataFrame(list(zip(continua+disc,ginis_nona,ginis_na_nl,max_val,P95,min_val,P5,mean_val,miss)),
             columns=['Var num','Gini sin nulos','Gini no linear','Max','P95','Min','P5','Mean','% Missing'])


# In[152]:


desc_num=pd.DataFrame(list(zip(continua+disc,ginis_nona,ginis_na_nl,max_val,P95,min_val,P5,mean_val,miss)),
             columns=['Var num','Gini sin nulos','Gini no linear','Max','P95','Min','P5','Mean','% Missing'])


# In[153]:


# Graficos bivariados de variables continuas

for ix in range(len(continua)):
    base_r=base.copy()
    base_r[continua[ix]+'d']=pd.qcut(base_r[continua[ix]], q=5,duplicates='drop')
    base_rx=base_r.groupby([continua[ix]+'d'],sort=True)['fraude2'].mean()
    plt.figure()
    base_rx.plot(kind='line',ylim=[0,0.5])
    plt.show()


# In[154]:


# Graficos bivariados de variables discretas

for ix in range(len(disc)):
    base_r=base.copy()
    base_r[disc[ix]+'d']=pd.qcut(base_r[disc[ix]], q=5,duplicates='drop')
    base_rx=base_r.groupby([disc[ix]+'d'],sort=True)['fraude2'].mean()
    plt.figure()
    base_rx.plot(kind='line',ylim=[0,0.5])
    plt.show()


# # Analisis variables categoricas

# In[155]:


ginis_na=[]
ctd_cat=[]
miss=[]
for cat in catg:
    newxtraincat=pd.get_dummies(X_train[cat])
    newxtestcat=pd.get_dummies(X_test[cat])
    clf=sm.Logit(y_train,newxtraincat).fit()
    gini = 2*metrics.roc_auc_score(y_test, clf.predict(newxtestcat)) -1
    ginis_na.append(gini*100)
    ctd_cat.append(len(newxtraincat.columns))
    miss.append((len(base.loc[base[cat].isna()==True])/len(base))*100)


# In[156]:


ginis_nona=[]
for cat in catg:
    newxtrain=X_train.loc[X_train[cat].isna()==False].copy()
    newytrain=y_train.drop(index=X_train.loc[X_train[cat].isna()==True].index)
    newxtest=X_test.loc[X_test[cat].isna()==False].copy()
    newytest=y_test.drop(index=X_test.loc[X_test[cat].isna()==True].index)
    
    newxtraincat=pd.get_dummies(newxtrain[cat])
    newxtestcat=pd.get_dummies(newxtest[cat])
    clf=sm.Logit(newytrain,newxtraincat).fit()
    gini = 2*metrics.roc_auc_score(newytest, clf.predict(newxtestcat)) -1
    ginis_nona.append(gini*100)


# In[157]:


pd.DataFrame(list(zip(catg,ginis_na,ginis_nona,ctd_cat,miss)),
             columns=['Var cat','Gini','Gini sin nulos','Ctd categor','% Missing'])


# In[158]:


for ix in range(len(catg)):
    base_r=base.loc[base[catg[ix]].isna()==False]
    base_rx=base_r.groupby([catg[ix]],sort=True)['fraude2'].mean()
    plt.figure()
    base_rx.plot(kind='bar',ylim=[0,0.5])
    plt.show()


# # Separacion en train test

# In[159]:


ids_target=base[['ID_USER','fraude2']].drop_duplicates(subset=['ID_USER','fraude2']).sort_values(by=['ID_USER'])
ids_target['idtarget']=ids_target['ID_USER'].astype(str)+ids_target['fraude2'].astype(str)


# In[160]:


idtrain, idtest,bor,bor = train_test_split(ids_target,ids_target['fraude2'],test_size=0.30,random_state=123,
                                           stratify=ids_target['fraude2'])


# In[161]:


print("rf train: " +str(idtrain['fraude2'].mean()))
print("rf test: " +str(idtest['fraude2'].mean()))


# In[162]:


base['idtarget']=base['ID_USER'].astype(str)+base['fraude2'].astype(str)
train_byidtarget=base.loc[base['idtarget'].isin(idtrain['idtarget'])].reset_index()
test_byidtarget=base.loc[base['idtarget'].isin(idtest['idtarget'])].reset_index()


# In[163]:


print("rf train: " +str(train_byidtarget['fraude2'].mean()))
print("rf test: " +str(test_byidtarget['fraude2'].mean()))


# # Acotamiento de variables numericas (outliers)

# In[164]:


# Cotas
COTAS=pd.DataFrame()
for i,j in  zip(continua+disc, range(len(train_byidtarget))):
    per5=np.percentile(train_byidtarget[i].dropna(),5)
    per95=np.percentile(train_byidtarget[i].dropna(),95)
    COTAS.loc[j,'VARIABLE']=i
    COTAS.loc[j,'PER5']=per5
    COTAS.loc[j,'PER95']=per95
COTAS.to_csv('COTAS.csv',index=False)


# In[165]:


COTAS=pd.read_csv("COTAS.csv",sep=',', encoding ='latin1')
def acotamiento(dfr):
    for i in range(len(COTAS)):
        VAR   = COTAS.loc[i,'VARIABLE']
        PER5  = COTAS.loc[i,'PER5']
        PER95 = COTAS.loc[i,'PER95']
        dfr[VAR+'_acot'] = dfr[VAR].apply(lambda x: PER5 if x < PER5 else 
                                        PER95 if x > PER95 else 
                                        x)                               
    return dfr
    
train_byidtarget_v2 = acotamiento(train_byidtarget)
test_byidtarget_v2 = acotamiento(test_byidtarget)


# In[166]:


numericas=continua+disc
numericas_2=[s + '_acot' for s in numericas]


# In[167]:


numericas_3=[]
for col in numericas_2:
    bor = train_byidtarget_v2[col]
    bor2 = bor.unique()
    if np.isnan(bor2).any()==True:
        if len(bor2)>2:
            numericas_3.append(col)
    else:
        if len(bor2)>1:
            numericas_3.append(col)


# # Correlacion

# In[168]:


corr_mtx = train_byidtarget_v2[numericas_3].corr().abs()


# In[169]:


grupos_saved=[]
grupos_discard=[]
for var in numericas_3:
    if var not in grupos_discard:
        grupos_saved.append(list(corr_mtx.loc[corr_mtx[var]>0.7].index))
        grupos_discard.extend(list(corr_mtx.loc[corr_mtx[var]>0.7].index))


# In[170]:


#Eleccion de mejores variables


# In[171]:


desc_num['Var num']=[s + '_acot' for s in desc_num['Var num']]


# In[176]:


grupos_saved


# In[173]:


desc_num.loc[desc_num['Var num'].isin(grupos_saved[0])] #r_monto_linea_acot


# In[178]:


desc_num.loc[desc_num['Var num'].isin(grupos_saved[9])] #ctd_operbyuser_acot


# In[179]:


numericas_4=['r_monto_linea_acot','montoant_acot','var_monto_acot','dia_acot','horas_slo_acot',
               'dcto_acot','hora_acot','ctd_operbyuser_acot','linea_tc_acot',
               'interes_tc_acot','ctd_oper0_acot']


# # Transformaciones numericas

# In[180]:


def TRANSFORMATION_A1(data,var,ruta):
    x=data[var]
    y=data['TARGET']
    #Tranforación Polinomial de 1 Grado
    def objective(x, a, b):
        return a * x + b
    popt, _ = curve_fit(objective, x, y)
    # summarize the parameter values
    a, b = popt
    # print('y = %.5f'%a +'*'+str(var)+'+ %.5f' % b)
    # plot input vs output
    y_line = objective(x, a, b)
    print(r2_score(y_line,y))
    name=str(var)+'_A1.png'
    # create a line plot for the mapping function
    fig, ax1 = plt.subplots()
    kwargs = dict(alpha=0.5, bins=100, histtype='bar')  
    ax1.hist(np.array(x),**kwargs)
    ax1.set_ylabel('Dist.'+ str(var))
    ax1.set_xlabel(var)
    ax2 = ax1.twinx()
    ax2.plot(np.array(x), np.array(y_line), '--', color='red', label=u'Transformation')
    ax2.scatter(x, y, label=u'Scatter plot')
    plt.title('Transformation Pol 1° '+ str(var))
    ax2.set_ylabel('Ratio de fraude2')
    ax2.legend(loc='upper right')
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(ruta + name) 
    plt.show()
    return a, b, r2_score(y_line,y)


# In[181]:


def TRANSFORMATION_A2(data,var,ruta):
    x=data[var]
    y=data['TARGET']
    #Tranforación Polinomial de 1 Grado
    def objective(x, a, b, c):
        return a * (x**2) + b*x +c
    popt, _ = curve_fit(objective, x, y)
    # summarize the parameter values
    a, b, c = popt
    # print('y = %.5f'%a +'*'+str(var)+'+ %.5f' % b)
    # plot input vs output
    y_line = objective(x, a, b, c)
    print(r2_score(y_line,y))
    name=str(var)+'_A2.png'
    # create a line plot for the mapping function
    fig, ax1 = plt.subplots()
    kwargs = dict(alpha=0.5, bins=100, histtype='bar')  
    ax1.hist(np.array(x),**kwargs)
    ax1.set_ylabel('Dist.'+ str(var))
    ax1.set_xlabel(var)
    ax2 = ax1.twinx()
    ax2.plot(np.array(x), np.array(y_line), '--', color='red', label=u'Transformation')
    ax2.scatter(x, y, label=u'Scatter plot')
    plt.title('Transformation Pol 2° '+ str(var))
    ax2.set_ylabel('Ratio de fraude2')
    ax2.legend(loc='upper right')
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(ruta + name) 
    plt.show()
    return a, b, c, r2_score(y_line,y)


# In[182]:


EQUATION_A1=pd.DataFrame()
borrar_1=train_byidtarget_v2.copy()
for i, j in zip(numericas_4, range(len(numericas_4))):
    borrar=borrar_1.loc[borrar_1[i].isna()==False]
    borrar[i +str('_bins')] = pd.qcut(x=borrar[i], q=50,duplicates='drop')
    group2=borrar.groupby([i +str('_bins')],sort=True)[[i,'fraude2']].mean().reset_index()
    group2['fraude2']=np.log(group2['fraude2']/(1-group2['fraude2']))
    group2.columns=[i +str('_var_bins'),i,'TARGET']
    group2
    a, b, r2 =TRANSFORMATION_A1(group2,i,'plots/')
    #Tranformacion lineal
    EQUATION_A1.loc[j,'VARIABLE']=i
    EQUATION_A1.loc[j,'a']=a
    EQUATION_A1.loc[j,'b']=b
    EQUATION_A1.loc[j,'r2']=r2
EQUATION_A1.to_csv('EQUATION_A1.csv',index=False)


# In[183]:


EQUATION_A2=pd.DataFrame()
borrar_1=train_byidtarget_v2.copy()
for i, j in zip(numericas_4, range(len(numericas_4))):
    borrar=borrar_1.loc[borrar_1[i].isna()==False]
    borrar[i +str('_bins')] = pd.qcut(x=borrar[i], q=50,duplicates='drop')
    group2=borrar.groupby([i +str('_bins')],sort=True)[[i,'fraude2']].mean().reset_index()
    group2['fraude2']=np.log(group2['fraude2']/(1-group2['fraude2']))
    group2.columns=[i +str('_var_bins'),i,'TARGET']
    group2
    a, b, c, r2 =TRANSFORMATION_A2(group2,i,'plots/')
    print
    #Tranformacion lineal
    EQUATION_A2.loc[j,'VARIABLE']=i
    EQUATION_A2.loc[j,'a']=a
    EQUATION_A2.loc[j,'b']=b
    EQUATION_A2.loc[j,'c']=c
    EQUATION_A2.loc[j,'r2']=r2
EQUATION_A2.to_csv('EQUATION_A2.csv',index=False)


# In[184]:


EQUATION_A1=pd.read_csv("EQUATION_A1.csv",sep=',', encoding ='latin1')
EQUATION_A2=pd.read_csv("EQUATION_A2.csv",sep=',', encoding ='latin1')


# In[185]:


EQUATION_A1[['VARIABLE','r2']]


# In[186]:


EQUATION_A2[['VARIABLE','r2']]


# In[187]:


def objective_A1(x, a, b):
    return a * x + b
def objective_A2(x, a, b, c):
    return a * (x**2) + b*x +c


# In[188]:


for i in range(len(EQUATION_A1)):
    train_byidtarget_v2[str(EQUATION_A1.iloc[i,0])+'_A1']=objective_A1(train_byidtarget_v2[EQUATION_A1.iloc[i,0]],EQUATION_A1.iloc[i,1],EQUATION_A1.iloc[i,2])
    test_byidtarget_v2[str(EQUATION_A1.iloc[i,0])+'_A1']=objective_A1(test_byidtarget_v2[EQUATION_A1.iloc[i,0]],EQUATION_A1.iloc[i,1],EQUATION_A1.iloc[i,2])


# In[189]:


for i in range(len(EQUATION_A2)):
    train_byidtarget_v2[str(EQUATION_A2.iloc[i,0])+'_A2']=objective_A2(train_byidtarget_v2[EQUATION_A2.iloc[i,0]],EQUATION_A2.iloc[i,1],EQUATION_A2.iloc[i,2],EQUATION_A2.iloc[i,3])
    test_byidtarget_v2[str(EQUATION_A2.iloc[i,0])+'_A2']=objective_A2(test_byidtarget_v2[EQUATION_A2.iloc[i,0]],EQUATION_A2.iloc[i,1],EQUATION_A2.iloc[i,2],EQUATION_A2.iloc[i,3])


# In[190]:


numericas_5=['r_monto_linea_acot_A2','montoant_acot_A1','var_monto_acot_A1','dia_acot_A2',
             'horas_slo_acot_A1','dcto_acot_A2','hora_acot_A1','ctd_operbyuser_acot_A1',
             'linea_tc_acot_A1','interes_tc_acot_A1','ctd_oper0_acot_A1']


# ## Missing numericas

# In[191]:


missing_input=[]
for rep in numericas_5:
    if len(train_byidtarget_v2.loc[train_byidtarget_v2[rep].isna()==True])==0:
        imput_val=train_byidtarget_v2['fraude2'].mean()
        imput_val2=np.log(imput_val/(1-imput_val))
        missing_input.append(imput_val2)
    else:
        imput_val=train_byidtarget_v2.loc[train_byidtarget_v2[rep].isna()==True]['fraude2'].mean()
        imput_val2=np.log(imput_val/(1-imput_val))
        missing_input.append(imput_val2)


# In[192]:


MISSING_VALUES=pd.DataFrame(list(zip(numericas_5,missing_input)),columns=['VARIABLE','MISSING_VAL'])
MISSING_VALUES.to_csv('missing_values.csv',index=False)


# In[193]:


missing_input=[]
for rep in catg:
    if len(train_byidtarget_v2.loc[train_byidtarget_v2[rep].isna()==True])==0:
        imput_val=train_byidtarget_v2['fraude2'].mean()
        imput_val2=np.log(imput_val/(1-imput_val))
        missing_input.append(imput_val2)
    else:
        imput_val=train_byidtarget_v2.loc[train_byidtarget_v2[rep].isna()==True]['fraude2'].mean()
        imput_val2=np.log(imput_val/(1-imput_val))
        missing_input.append(imput_val2)


# In[194]:


MISSING_VALUESCAT=pd.DataFrame(list(zip(catg,missing_input)),columns=['VARIABLE','MISSING_VAL'])
MISSING_VALUESCAT.to_csv('missing_valuescat.csv',index=False)


# # Transformacion categoricas

# In[195]:


categories=[]
for i in range(len(catg)):
    values=list(train_byidtarget_v2.loc[train_byidtarget_v2[catg[i]].isna()==False][catg[i]].unique())
    cattransf=[]
    for val in values:
        imput_val=train_byidtarget_v2.loc[train_byidtarget_v2[catg[i]]==val]['fraude2'].mean()
        imput_val2=np.log(imput_val/(1-imput_val))
        cattransf.append([val,imput_val2])
    categories.append(cattransf)


# In[196]:


for i in range(len(catg)):
    values=list(train_byidtarget_v2.loc[train_byidtarget_v2[catg[i]].isna()==False][catg[i]].unique())
    for j in range(len(values)):
        train_byidtarget_v2.loc[train_byidtarget_v2[catg[i]]==values[j],catg[i]+'_st']=categories[i][j][1]
        test_byidtarget_v2.loc[test_byidtarget_v2[catg[i]]==values[j],catg[i]+'_st']=categories[i][j][1]


# ## Missing categoricas

# In[198]:


for i in range(len(catg)):
    train_byidtarget_v2.loc[train_byidtarget_v2[catg[i]].isna()==True,catg[i]+'_st']=MISSING_VALUESCAT['MISSING_VAL'][i]
    test_byidtarget_v2.loc[test_byidtarget_v2[catg[i]].isna()==True,catg[i]+'_st']=MISSING_VALUESCAT['MISSING_VAL'][i]
for i in range(len(numericas_5)):
    train_byidtarget_v2.loc[train_byidtarget_v2[numericas_5[i]].isna()==True,numericas_5[i]]=MISSING_VALUES['MISSING_VAL'][i]
    test_byidtarget_v2.loc[test_byidtarget_v2[numericas_5[i]].isna()==True,numericas_5[i]]=MISSING_VALUES['MISSING_VAL'][i]


# In[199]:


catg_1=[s + '_st' for s in catg]


# # Seleccion de variables

# In[200]:


y2= train_byidtarget_v2['fraude2']
data2= train_byidtarget_v2[numericas_5+catg_1]


# In[201]:


def timer(start_time=None):
    if not start_time:
        start_time = datetime.now()
        return start_time
    elif start_time:
        thour, temp_sec = divmod((datetime.now() - start_time).total_seconds(), 3600)
        tmin, tsec = divmod(temp_sec, 60)
        print('\n Time taken: %i hours %i minutes and %s seconds.' % (thour, tmin, round(tsec, 2)))


# In[202]:


from sklearn.ensemble import RandomForestClassifier


# In[203]:


# Seleccion por boruta
rfc = RandomForestClassifier(n_estimators=100, n_jobs=-1, class_weight='balanced', max_depth=5,criterion='gini')
boruta_selector = BorutaPy(rfc, n_estimators='auto', verbose=2)
start_time = timer(None)
boruta_selector.fit(data2.values, y2)
timer(start_time)


# In[205]:


pd.DataFrame(list(zip(numericas_5+catg_1,boruta_selector.support_)),
             columns=['Var','Selected']).sort_values(by=['Selected'],ascending=False)


# In[206]:


selection_var=pd.DataFrame(list(zip(numericas_5+catg_1,boruta_selector.support_)),
             columns=['Var','Selected']).sort_values(by=['Selected'],ascending=False)
var_finales=list(selection_var.loc[selection_var['Selected']==True]['Var'])


# In[524]:


# Metodo descartado, se utilizo porque el boruta no encontraba variables importantes, por ende se introdujo un forzaje
"""
# ANOVA feature selection for numeric input and categorical output
from sklearn.datasets import make_classification
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_classif
# define feature selection
fs = SelectKBest(score_func=f_classif, k=10)
# apply feature selection
X_selected = fs.fit(data2, y2)

#pd.DataFrame(list(zip(numericas_5+catg_1,X_selected.get_support())),
#             columns=['Var','Selected']).sort_values(by=['Selected'],ascending=False)
"""

"""selection_var=pd.DataFrame(list(zip(numericas_5+catg_1,X_selected.get_support())),
             columns=['Var','Selected']).sort_values(by=['Selected'],ascending=False)
var_finales=list(selection_var.loc[selection_var['Selected']==True]['Var'])"""


# # Modelamiento

# ## Modelos lineales
# 
# Prueba lineal con una regresion logistica y una pequeña grilla

# In[208]:


kfold = StratifiedKFold(n_splits = 5, shuffle = True, random_state = 123)
model = LogisticRegression(random_state= 123, solver='lbfgs', max_iter=5000,penalty='l2')

grid = {"C": [0.001, 0.01,0.1,0.5,1],
       'class_weight': ['balanced',{True:0.5,False:0.5}]}

gs = GridSearchCV(model, param_grid = grid, cv = kfold, return_train_score=True, scoring = 'recall',verbose = 2)
gs.fit(train_byidtarget_v2[var_finales],train_byidtarget_v2['fraude2'])


# In[209]:


gs.best_estimator_


# In[210]:


#gini train
probs = gs.best_estimator_.predict_proba(train_byidtarget_v2[var_finales])
gini = 2*metrics.roc_auc_score(train_byidtarget_v2['fraude2'], probs[:,1])-1
gini


# In[211]:


#gini test
probs = gs.best_estimator_.predict_proba(test_byidtarget_v2[var_finales])
gini = 2*metrics.roc_auc_score(test_byidtarget_v2['fraude2'], probs[:,1])-1
gini


# ## MODELOS NO LINEALES

# ### Decision tree
# 
# Prueba con un arbol de decisiones para conocer el performance

# In[212]:


model = DecisionTreeClassifier(random_state = 123)
max_depth=5
min_sample_split=24
min_sample_leaf=6
grid = {"max_depth": [max_depth-1, max_depth, max_depth+1],
        "max_features": ['auto', 'log2'],
        "min_samples_split": [min_sample_split-2, min_sample_split, min_sample_split+2],
        "min_samples_leaf": [min_sample_leaf],
        "criterion": ["gini", 'entropy'],
        'class_weight': ['balanced',None],
        'max_features': [0.6,0.8,1]
        }
gs = GridSearchCV(model, param_grid = grid, cv = kfold, n_jobs=-1, return_train_score=True, scoring = 'roc_auc',verbose=2)
gs.fit(train_byidtarget_v2[var_finales],train_byidtarget_v2['fraude2'])


# In[213]:


gs.best_estimator_


# In[214]:


pd.DataFrame(list(zip(numericas_5+catg_1,gs.best_estimator_.feature_importances_)),
             columns=['Var','Importance']).sort_values(by=['Importance'],ascending=False)


# In[215]:


#gini train
probs = gs.best_estimator_.predict_proba(train_byidtarget_v2[var_finales])
gini = 2*metrics.roc_auc_score(train_byidtarget_v2['fraude2'], probs[:,1])-1
gini


# In[216]:


#gini test
probs = gs.best_estimator_.predict_proba(test_byidtarget_v2[var_finales])
gini = 2*metrics.roc_auc_score(test_byidtarget_v2['fraude2'], probs[:,1])-1
gini


# ### Lightgbm
# 
# Prueba con un modelo de machine learning para conocer el performance

# In[217]:


ids_target2=train_byidtarget_v2[['ID_USER','fraude2']].drop_duplicates(subset=['ID_USER','fraude2']).sort_values(by=['ID_USER'])
ids_target2['idtarget']=ids_target2['ID_USER'].astype(str)+ids_target2['fraude2'].astype(str)


# In[527]:


# spliteo al train en watch list para el refuerzo


# In[218]:


idtrain2, idtest2,bor,bor = train_test_split(ids_target2,ids_target2['fraude2'],test_size=0.20,random_state=123,
                                           stratify=ids_target2['fraude2'])


# In[219]:


print("rf train: " +str(idtrain2['fraude2'].mean()))
print("rf test: " +str(idtest2['fraude2'].mean()))


# In[220]:


train=train_byidtarget_v2.loc[train_byidtarget_v2['idtarget'].isin(idtrain2['idtarget'])].reset_index()
watch=train_byidtarget_v2.loc[train_byidtarget_v2['idtarget'].isin(idtest2['idtarget'])].reset_index()


# In[221]:


print("len train: " +str(train['fraude2'].count()))
print("len watch: " +str(watch['fraude2'].count()))


# In[222]:


print("rf train: " +str(train['fraude2'].mean()))
print("rf test: " +str(watch['fraude2'].mean()))


# In[223]:


kfold = StratifiedKFold(n_splits = 3, shuffle = True, random_state = 123)
model = lgb.LGBMClassifier(num_boost_round=500,verbose_eval="False",random_state=123,
                           metric='auc',eval_metric='auc')
max_depth=3
num_leaves=64
learning_rate=0.1
max_bin=128
bagging=0.8
grid = {'boosting_type': ['gbdt'],
        'objective': ['binary'],
        'max_depth':[max_depth+1,max_depth],
        'num_leaves':[num_leaves],
        'learning_rate': [learning_rate],
        #'min_child_samples': [min_child], #1740 un 3% del total de los datos
        #'min_child_weight': [100],
        'lambda_l2':[0,0.3],
        'is_unbalance':['true'],
        'max_bin':[max_bin],
        'bagging_fraction':[bagging]
        }
gs = GridSearchCV(model, param_grid = grid, cv = kfold, refit = 'auc', n_jobs=-1, return_train_score=True)

gs.fit(train[var_finales],train['fraude2'],eval_set=[(watch[var_finales],watch['fraude2'])],
       early_stopping_rounds=200,eval_metric='auc',verbose=True)


# In[224]:


probs = gs.best_estimator_.predict_proba(train[var_finales])
gini = 2*metrics.roc_auc_score(train['fraude2'], probs[:,1])-1
gini


# In[225]:


probs = gs.best_estimator_.predict_proba(watch[var_finales])
gini = 2*metrics.roc_auc_score(watch['fraude2'], probs[:,1])-1
gini


# In[226]:


probs = gs.best_estimator_.predict_proba(test_byidtarget_v2[var_finales])
gini = 2*metrics.roc_auc_score(test_byidtarget_v2['fraude2'], probs[:,1])-1
gini


# # Seleccion del modelo final

# In[227]:


max_depth=[2,3,4,5,6,7] 
min_sample_split=[4,8,12,24,36] 
min_sample_leaf=[4,8,12,24,36]
class_weight=[None] # Se quito el metodo 'balanced', debido a que la distribucion ya no es tan distante
max_features=[1,0.8,0.5]


# In[228]:


fs_results = pd.DataFrame(columns = ['max_depth', 'min_sample_split', 'min_sample_leaf','class_weight','max_features'])
for md,mss,msl,cw,mf in itertools.product(max_depth, min_sample_split,min_sample_leaf,class_weight,max_features):
    row = {'max_depth':md, 'min_sample_split':mss,'min_sample_leaf':msl,'class_weight':cw,'max_features':mf}
    fs_results = fs_results.append(row, ignore_index=True)
del md,mss,msl,cw,mf


# In[229]:


fs_results


# In[230]:


fs_results['max_depth']=fs_results['max_depth'].astype(int)
fs_results['min_sample_split']=fs_results['min_sample_split'].astype(int)
fs_results['min_sample_leaf']=fs_results['min_sample_leaf'].astype(int)
fs_results['max_features']=fs_results['max_features'].astype(float)


# In[232]:


gini_train=[]
gini_test=[]
gini_total=[]
for cont in range(len(fs_results)):
    if type(fs_results['class_weight'][cont])==np.float64:
        model = DecisionTreeClassifier(random_state = 123)
        grid = {"max_depth": [fs_results['max_depth'][cont]],
                "max_features": ['auto', 'log2'],
                "min_samples_split": [fs_results['min_sample_split'][cont]],
                "min_samples_leaf": [fs_results['min_sample_leaf'][cont]],
                "criterion": ["gini", 'entropy'],
                'class_weight': [None],
                'max_features': [fs_results['max_features'][cont]]
                }
        gs = GridSearchCV(model, param_grid = grid, cv = kfold, n_jobs=-1, return_train_score=True, scoring = 'roc_auc',verbose=2)
        gs.fit(train_byidtarget_v2[var_finales],train_byidtarget_v2['fraude2'])
        probs = gs.best_estimator_.predict_proba(train_byidtarget_v2[var_finales])
        gini = 2*metrics.roc_auc_score(train_byidtarget_v2['fraude2'], probs[:,1])-1
        gini_train.append(gini)
        probs = gs.best_estimator_.predict_proba(test_byidtarget_v2[var_finales])
        gini = 2*metrics.roc_auc_score(test_byidtarget_v2['fraude2'], probs[:,1])-1
        gini_test.append(gini)
        print(cont)
        total_byidtarget_v2 = pd.concat([train_byidtarget_v2,test_byidtarget_v2])
        probs = gs.best_estimator_.predict_proba(total_byidtarget_v2[var_finales])
        gini = 2*metrics.roc_auc_score(total_byidtarget_v2['fraude2'], probs[:,1])-1
        gini_total.append(gini)
    else:
        model = DecisionTreeClassifier(random_state = 123)
        grid = {"max_depth": [fs_results['max_depth'][cont]],
                "max_features": ['auto', 'log2'],
                "min_samples_split": [fs_results['min_sample_split'][cont]],
                "min_samples_leaf": [fs_results['min_sample_leaf'][cont]],
                "criterion": ["gini", 'entropy'],
                'class_weight': [fs_results['class_weight'][cont]],
                'max_features': [fs_results['max_features'][cont]]
                }
        gs = GridSearchCV(model, param_grid = grid, cv = kfold, n_jobs=-1, return_train_score=True, scoring = 'roc_auc',verbose=2)
        gs.fit(train_byidtarget_v2[var_finales],train_byidtarget_v2['fraude2'])
        probs = gs.best_estimator_.predict_proba(train_byidtarget_v2[var_finales])
        gini = 2*metrics.roc_auc_score(train_byidtarget_v2['fraude2'], probs[:,1])-1
        gini_train.append(gini)
        probs = gs.best_estimator_.predict_proba(test_byidtarget_v2[var_finales])
        gini = 2*metrics.roc_auc_score(test_byidtarget_v2['fraude2'], probs[:,1])-1
        gini_test.append(gini)
        print(cont)
        total_byidtarget_v2 = pd.concat([train_byidtarget_v2,test_byidtarget_v2])
        probs = gs.best_estimator_.predict_proba(total_byidtarget_v2[var_finales])
        gini = 2*metrics.roc_auc_score(total_byidtarget_v2['fraude2'], probs[:,1])-1
        gini_total.append(gini)      
fs_results['gini_train']=gini_train
fs_results['gini_test']=gini_test
fs_results['gini_total']=gini_total


# In[ ]:


# Decido quedarme con el modelo que mejor performance en test tenga, debido a que se que es el que mejor extrapolara en el futuro


# In[233]:


fs_results.sort_values(by=['gini_test','gini_total','gini_train'],ascending=False)


# In[234]:


if type(fs_results['class_weight'][150])==np.float64:
    model = DecisionTreeClassifier(random_state = 123)
    grid = {"max_depth": [fs_results['max_depth'][150]],
            "max_features": ['auto', 'log2'],
            "min_samples_split": [fs_results['min_sample_split'][150]],
            "min_samples_leaf": [fs_results['min_sample_leaf'][150]],
            "criterion": ["gini", 'entropy'],
            'class_weight': [None],
            'max_features': [fs_results['max_features'][150]]
            }
    gs = GridSearchCV(model, param_grid = grid, cv = kfold, n_jobs=-1, return_train_score=True, scoring = 'roc_auc',verbose=2)
    gs.fit(train_byidtarget_v2[var_finales],train_byidtarget_v2['fraude2'])
else:
    model = DecisionTreeClassifier(random_state = 123)
    grid = {"max_depth": [fs_results['max_depth'][150]],
            "max_features": ['auto', 'log2'],
            "min_samples_split": [fs_results['min_sample_split'][150]],
            "min_samples_leaf": [fs_results['min_sample_leaf'][150]],
            "criterion": ["gini", 'entropy'],
            'class_weight': [fs_results['class_weight'][150]],
            'max_features': [fs_results['max_features'][150]]
            }
    gs = GridSearchCV(model, param_grid = grid, cv = kfold, n_jobs=-1, return_train_score=True, scoring = 'roc_auc',verbose=2)
    gs.fit(train_byidtarget_v2[var_finales],train_byidtarget_v2['fraude2'])


# In[236]:


probs = gs.best_estimator_.predict_proba(train_byidtarget_v2[var_finales])
gini = 2*metrics.roc_auc_score(train_byidtarget_v2['fraude2'], probs[:,1])-1
print(gini)
probs = gs.best_estimator_.predict_proba(test_byidtarget_v2[var_finales])
gini = 2*metrics.roc_auc_score(test_byidtarget_v2['fraude2'], probs[:,1])-1
print(gini)
total_byidtarget_v2 = pd.concat([train_byidtarget_v2,test_byidtarget_v2])
probs = gs.best_estimator_.predict_proba(total_byidtarget_v2[var_finales])
gini = 2*metrics.roc_auc_score(total_byidtarget_v2['fraude2'], probs[:,1])-1
print(gini)


# In[237]:


gs.best_estimator_


# # Principales resultados

# In[238]:


pd.DataFrame(list(zip(var_finales,gs.best_estimator_.feature_importances_)),
             columns=['Var','Importance']).sort_values(by=['Importance'],ascending=False)


# In[500]:


probs = gs.best_estimator_.predict_proba(total_byidtarget_v2[var_finales])
fpr, tpr, thresholds=metrics.roc_curve(total_byidtarget_v2['fraude2'],  probs[:,1])


# In[501]:


#fpr, tpr, threshold = metrics.roc_curve(y_test, preds)
roc_auc = metrics.auc(fpr, tpr)

# method I: plt
import matplotlib.pyplot as plt
plt.title('Receiver Operating Characteristic')
plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
plt.legend(loc = 'lower right')
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0, 1])
plt.ylim([0, 1])
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.show()


# In[502]:


# Calculate the G-mean
gmean = np.sqrt(tpr * (1 - fpr))

# Find the optimal threshold
index = np.argmax(gmean)
thresholdOpt = round(thresholds[index], ndigits = 4)
gmeanOpt = round(gmean[index], ndigits = 4)
fprOpt = round(fpr[index], ndigits = 4)
tprOpt = round(tpr[index], ndigits = 4)
print('Best Threshold: {} with G-Mean: {}'.format(thresholdOpt, gmeanOpt))
print('FPR: {}, TPR: {}'.format(fprOpt, tprOpt))


# In[241]:


y,class_names = pd.factorize(total_byidtarget_v2['fraude2'])


# In[242]:



feature_names = var_finales
dot_data = tree.export_graphviz(gs.best_estimator_, out_file=None, filled=True, rounded = True, feature_names=feature_names)
graph = graphviz.Source(dot_data)
graph


# In[334]:


probs = gs.best_estimator_.predict_proba(train_byidtarget_v2[var_finales])[:,1]
train_byidtarget_v2['PF']=probs
probs = gs.best_estimator_.predict_proba(test_byidtarget_v2[var_finales])[:,1]
test_byidtarget_v2['PF']=probs
probs = gs.best_estimator_.predict_proba(total_byidtarget_v2[var_finales])[:,1]
total_byidtarget_v2['PF']=probs


# In[341]:


total_byidtarget_v2=total_byidtarget_v2.reset_index(drop=True)


# In[385]:


newpf=[]
for a in range(len(total_byidtarget_v2)):
    if total_byidtarget_v2['PF'][a]==0:
        newpf.append(0.001)
    elif total_byidtarget_v2['PF'][a]==1:
        newpf.append(0.999)
    else:
        newpf.append(total_byidtarget_v2['PF'][a])
total_byidtarget_v2['PF_NEW']=newpf


# In[386]:


newpf=[]
for a in range(len(train_byidtarget_v2)):
    if train_byidtarget_v2['PF'][a]==0:
        newpf.append(0.001)
    elif train_byidtarget_v2['PF'][a]==1:
        newpf.append(0.999)
    else:
        newpf.append(train_byidtarget_v2['PF'][a])
train_byidtarget_v2['PF_NEW']=newpf


# In[387]:


newpf=[]
for a in range(len(test_byidtarget_v2)):
    if test_byidtarget_v2['PF'][a]==0:
        newpf.append(0.001)
    elif test_byidtarget_v2['PF'][a]==1:
        newpf.append(0.999)
    else:
        newpf.append(test_byidtarget_v2['PF'][a])
test_byidtarget_v2['PF_NEW']=newpf


# # Calibrado de nivel

# In[394]:


XB=-np.log((1/total_byidtarget_v2['PF_NEW'])-1)
total_byidtarget_v2['xb_pfnew']=XB
XB=-np.log((1/train_byidtarget_v2['PF_NEW'])-1)
train_byidtarget_v2['xb_pfnew']=XB
XB=-np.log((1/test_byidtarget_v2['PF_NEW'])-1)
test_byidtarget_v2['xb_pfnew']=XB


# In[395]:



calibrada = LogisticRegression(random_state=123).fit(X=total_byidtarget_v2[['xb_pfnew']], y=total_byidtarget_v2['fraude2'])


# In[412]:


train_byidtarget_v2['PF_CALIB']=calibrada.predict_proba(train_byidtarget_v2[['xb_pfnew']])[:,1]
test_byidtarget_v2['PF_CALIB']=calibrada.predict_proba(test_byidtarget_v2[['xb_pfnew']])[:,1]
total_byidtarget_v2['PF_CALIB']=calibrada.predict_proba(total_byidtarget_v2[['xb_pfnew']])[:,1]


# # Analisis de resultados del modelo

# In[431]:


dias=total_byidtarget_v2['dia'].unique()
dias.sort()
gini_target=[]
n_dia=[]
pf_dia=[]
rf_dia=[]
for a in dias:
    dates=total_byidtarget_v2.loc[total_byidtarget_v2['dia']==a]
    gini= 2*metrics.roc_auc_score(dates['fraude2'], dates['PD_CALIB'])-1
    gini_target.append(gini)
    n_dia.append(len(dates))
    pf_dia.append(dates['PF_CALIB'].mean())
    rf_dia.append(dates['fraude2'].mean())
    DISTRIBUCION=pd.DataFrame(list(zip(dias,gini_target,n_dia,
                                       pf_dia,rf_dia)),columns=['dia','gini_TARGET','n_dia','pf_dia','rf_dia'])
DISTRIBUCION


# ## Distribucion del target y prediccion

# In[490]:


fig, ax1 = plt.subplots()

color = 'tab:gray'
ax1.set_xlabel('dia')
ax1.set_ylabel('Cantidad', color=color)
ax1.bar(DISTRIBUCION['dia'], DISTRIBUCION['n_dia'], color=color,label='Cantidad')
ax1.tick_params(axis='y', labelcolor=color)
ax1.legend()
ax2 = ax1.twinx()  

color = 'tab:red'
ax2.set_ylabel('PF', color=color)  # we already handled the x-label with ax1
ax2.plot(DISTRIBUCION['dia'], DISTRIBUCION['pf_dia'], color=color,label='pf')
ax2.tick_params(axis='y', labelcolor=color)
ax2.set_ylim([0,0.5])
ax2.legend()
ax3 = ax1.twinx()

color = 'tab:blue'
ax3.set_ylabel('RF', color=color)  # we already handled the x-label with ax1
ax3.plot(DISTRIBUCION['dia'], DISTRIBUCION['rf_dia'], color=color,label='rf')
ax3.tick_params(axis='y', labelcolor=color)
ax3.set_ylim([0,0.5])
ax3.legend()
#fig.tight_layout() 

plt.show()


# # Distribucion de las variables

# In[466]:


total_byidtarget_v2['ctd_operbyuserd']=pd.qcut(total_byidtarget_v2['ctd_operbyuser'], q=10,duplicates='drop')
dist_var=total_byidtarget_v2.groupby(['ctd_operbyuserd'],sort=True)['fraude2','PF_CALIB'].mean().reset_index()
dist_var.plot(x='ctd_operbyuserd',y=['fraude2','PF_CALIB'],kind='line',rot=45,ylim=([0,0.6]))


# In[467]:


total_byidtarget_v2['linea_tcd']=pd.qcut(total_byidtarget_v2['linea_tc'], q=10,duplicates='drop')
dist_var=total_byidtarget_v2.groupby(['linea_tcd'],sort=True)['fraude2','PF_CALIB'].mean().reset_index()
dist_var.plot(x='linea_tcd',y=['fraude2','PF_CALIB'],kind='line',rot=45,ylim=([0,0.6]))


# In[468]:


total_byidtarget_v2['horas_slod']=pd.qcut(total_byidtarget_v2['horas_slo'], q=10,duplicates='drop')
dist_var=total_byidtarget_v2.groupby(['horas_slod'],sort=True)['fraude2','PF_CALIB'].mean().reset_index()
dist_var.plot(x='horas_slod',y=['fraude2','PF_CALIB'],kind='line',rot=45,ylim=([0,0.6]))


# In[471]:


total_byidtarget_v2['diad']=pd.qcut(total_byidtarget_v2['dia'], q=10,duplicates='drop')
dist_var=total_byidtarget_v2.groupby(['diad'],sort=True)['fraude2','PF_CALIB'].mean().reset_index()
dist_var.plot(x='diad',y=['fraude2','PF_CALIB'],kind='line',rot=45,ylim=([0,0.6]))


# In[470]:


dist_var=total_byidtarget_v2.groupby(['genero'],sort=True)['fraude2','PF_CALIB'].mean().reset_index()
dist_var.plot(x='genero',y=['fraude2','PF_CALIB'],kind='bar',rot=45,ylim=([0,0.6]))


# # Trade off modelo uso y no uso

# In[503]:


tresh=0.283 #Punto de corte optimo
total_byidtarget_v2.loc[total_byidtarget_v2['PF_CALIB']>=tresh,'PRED_TRESH']=1
total_byidtarget_v2.loc[total_byidtarget_v2['PF_CALIB']<tresh,'PRED_TRESH']=0


# Teniendo como contexto el uso o no uso del modelo, partamos por definir que todo aquella operacion que ha sido fraude tiene como perdida el monto de esa operacion y toda aquella operacion que no ha sido fraude tiene como ganancia el interes de esa operacion.

# In[522]:


perdida0=total_byidtarget_v2.loc[(total_byidtarget_v2['fraude']==0)]['interes_tc'].sum()
ganancia0=total_byidtarget_v2.loc[(total_byidtarget_v2['fraude']==1)]['monto'].sum()


# In[580]:


# Utilidad
ganancia0-perdida0


# Ahora si se utilizase el modelo, habria que considerar como perdida el monto de aquellas operaciones que no fueron fraude, pero que el modelo (dado el punto de corte optimo) considera que si serian fraude en un futuro. Tambien son perdida el interes de aquellas operaciones que han sido fraude y el modelo no detecto que lo fueran asi.
# Las ganancias, en cambio, son el monto de operaciones que fueron fraude y el modelo detecto que asi lo serian. Por ultimo el segundo tipo de ganancias serian aquellas operaciones que el modelo predijo que no serian fraude y efectivamente no lo fueron.

# In[581]:


perdida1=total_byidtarget_v2.loc[(total_byidtarget_v2['fraude']==0)&(total_byidtarget_v2['PRED_TRESH']==1)]['interes_tc'].sum()
perdida2=total_byidtarget_v2.loc[(total_byidtarget_v2['fraude']==1)&(total_byidtarget_v2['PRED_TRESH']==0)]['monto'].sum()
ganancia1=total_byidtarget_v2.loc[(total_byidtarget_v2['fraude']==1)&(total_byidtarget_v2['PRED_TRESH']==1)]['monto'].sum()
ganancia2=total_byidtarget_v2.loc[(total_byidtarget_v2['fraude']==0)&(total_byidtarget_v2['PRED_TRESH']==0)]['interes_tc'].sum()


# In[582]:


# Utilidad
(ganancia2+ganancia1)-(perdida1+perdida2)


# Debido a que no se tiene un diccionario, vamos a tomar un supuesto respecto a la variables 'status_txn', la cual se asume que es el status de la transaccion. Con ello las formulas anteriores propuestas cambian tomando en cuenta si la operacion se encuentra aceptada, rechazada o en proceso.
# 
# Vamos a excluir del analisis las operaciones 'en proceso'.
# 
# **Se pueden rechazar a clientes por multiples motivos no necesariamente asociados al fraude. Vamos a simplificar el analisis y asumir que todos los clientes rechazados han sido porque se creia que harian fraude**

# In[589]:


analisis1 = total_byidtarget_v2.loc[total_byidtarget_v2['status_txn']!='En proceso']


# In[593]:


perdida3=analisis1.loc[(analisis1['fraude']==1)&(analisis1['status_txn']=='Aceptada')]['monto'].sum()
perdida4=analisis1.loc[(analisis1['fraude']==0)&(analisis1['status_txn']=='Rechazada')]['interes_tc'].sum()
ganancia3=analisis1.loc[(analisis1['fraude']==0)&(analisis1['status_txn']=='Aceptada')]['interes_tc'].sum()
ganancia4=analisis1.loc[(analisis1['fraude']==1)&(analisis1['status_txn']=='Rechazada')]['monto'].sum()


# In[594]:


# Utilidad normal
(ganancia3+ganancia4)-(perdida3+perdida4) 


# In[596]:


perdida1=analisis1.loc[(analisis1['fraude']==0)&(analisis1['PRED_TRESH']==1)]['interes_tc'].sum()
perdida2=analisis1.loc[(analisis1['fraude']==1)&(analisis1['PRED_TRESH']==0)]['monto'].sum()
ganancia1=analisis1.loc[(analisis1['fraude']==1)&(analisis1['PRED_TRESH']==1)]['monto'].sum()
ganancia2=analisis1.loc[(analisis1['fraude']==0)&(analisis1['PRED_TRESH']==0)]['interes_tc'].sum()


# In[597]:


# Utilidad del modelo
(ganancia2+ganancia1)-(perdida1+perdida2)


# Se observa que el modelo, performa peor que una clasificacion simple acerca de la transaccion. Esto es debido a que, en primera instancia, el que la operacion haga fraude o no, no es el target del modelo. El target del modelo es detectar si el cliente hara fraude en un futuro (*mayor explicacion en la guia metodologica*). Si buscamos la deteccion de clientes que haran fraude y lo llevamos eso al analisis podemos hacer una comparacion mas justa para este.
# 
# Como ultimo punto, las operaciones que no han sido fraude, pero que el cliente tendra un futuro fraude con una operacion diferente, no se considerara como perdida o ganancia el monto de la operacion, sino mas bien los intereses (no se busca tener clientes en el portafolio que cometan fraude).

# In[615]:


perdida3=analisis1.loc[(analisis1['fraude']==1)&(analisis1['status_txn']=='Aceptada')]['monto'].sum()
perdida4=analisis1.loc[(analisis1['fraude']==0)&(analisis1['fraude2']==1)
                       &(analisis1['status_txn']=='Aceptada')]['interes_tc'].sum()
perdida5=analisis1.loc[(analisis1['fraude2']==0)&(analisis1['status_txn']=='Rechazada')]['interes_tc'].sum()
ganancia3=analisis1.loc[(analisis1['fraude2']==0)&(analisis1['status_txn']=='Aceptada')]['interes_tc'].sum()
ganancia4=analisis1.loc[(analisis1['fraude']==1)&(analisis1['status_txn']=='Rechazada')]['monto'].sum()
ganancia5=analisis1.loc[(analisis1['fraude']==0)&(analisis1['fraude2']==1)
                        &(analisis1['status_txn']=='Rechazada')]['interes_tc'].sum()


# In[616]:


# Utilidad normal
(ganancia3+ganancia4+ganancia5)-(perdida3+perdida4+perdida5) 


# In[617]:


perdida3=analisis1.loc[(analisis1['fraude']==1)&(analisis1['PRED_TRESH']==0)]['monto'].sum()
perdida4=analisis1.loc[(analisis1['fraude']==0)&(analisis1['fraude2']==1)
                       &(analisis1['PRED_TRESH']==0)]['interes_tc'].sum()
perdida5=analisis1.loc[(analisis1['fraude2']==0)&(analisis1['PRED_TRESH']==1)]['interes_tc'].sum()
ganancia3=analisis1.loc[(analisis1['fraude2']==0)&(analisis1['PRED_TRESH']==0)]['interes_tc'].sum()
ganancia4=analisis1.loc[(analisis1['fraude']==1)&(analisis1['PRED_TRESH']==1)]['monto'].sum()
ganancia5=analisis1.loc[(analisis1['fraude']==0)&(analisis1['fraude2']==1)
                        &(analisis1['PRED_TRESH']==1)]['interes_tc'].sum()


# In[618]:


# Utilidad normal
(ganancia3+ganancia4+ganancia5)-(perdida3+perdida4+perdida5) 


# El modelo presenta mejor discriminacion en utilidad tomando los supuestos considerados anteriormente.

# # Anexos

# ### 1. Categorizacion de clientes y descripcion de estos

# In[531]:


# Cantidad de clientes en el mes
len(total_byidtarget_v2['ID_USER'].unique())


# In[546]:


dif_clidia=[]
dia=[]
for a in range(3,31):
    list_rep=list(total_byidtarget_v2.loc[total_byidtarget_v2['dia']<a]['ID_USER'].unique())
    list_dia=list(total_byidtarget_v2.loc[total_byidtarget_v2['dia']==a]['ID_USER'].unique())
    dif_clidia.append(len(set(list_dia) - set(list_rep)))
    dia.append(a)


# In[550]:


# Conocer cuantos clientes nuevos llegan por dia
pd.DataFrame(list(zip(dia,dif_clidia)),
             columns=['dia','Clientes diferentes x dia']).plot(x='dia',
                                                               y='Clientes diferentes x dia',
                                                               kind='line',
                                                               rot=45)


# Los clientes nuevos tienden a rondar en los primeros dias del mes.

# In[567]:


# Conocer cuantas operaciones tiene un cliente en el mes
cantidad_oper=total_byidtarget_v2.groupby(['ID_USER'],sort=True)[['ctd_operbyuser']].max().reset_index()
cantidad_oper.groupby(['ctd_operbyuser'],sort=True).count().reset_index().plot(x='ctd_operbyuser',
                                                                               y='ID_USER',
                                                                               kind='line',
                                                                               rot=90)


# El 80% de los clientes hacen menos de 10 operaciones por mes y un 15% solo hace una operacion en el mes.

# In[576]:


# Conocer la distribucione de hombres y mujeres por cliente
genero_cli=total_byidtarget_v2[['ID_USER','genero']].drop_duplicates(subset=["ID_USER", "genero"])
genero_cli.groupby(['genero'],sort=True).count().reset_index().plot(x='genero',
                                                                               y='ID_USER',
                                                                               kind='bar')


# ### 2. Si no se splitea por cliente el modelo tenderia a overfitear, se puede observar que el performance en test, es incluso mejor que el performance en train, esto se da debido a que las observaciones no son del todo independientes entre si.

# In[249]:


total_byidtarget_v2 = pd.concat([train_byidtarget_v2,test_byidtarget_v2])


# In[250]:


trainprueba, testprueba = train_test_split(total_byidtarget_v2,test_size=0.30,random_state=123,
                                           stratify=total_byidtarget_v2['fraude2'])


# In[251]:


kfold = StratifiedKFold(n_splits = 5, shuffle = True, random_state = 123)
model = LogisticRegression(random_state= 123, solver='lbfgs', max_iter=5000,penalty='l2')

grid = {"C": [0.001, 0.01,0.1,0.5,1],
       'class_weight': ['balanced',{True:0.5,False:0.5}]}

gs = GridSearchCV(model, param_grid = grid, cv = kfold, return_train_score=True, scoring = 'recall',verbose = 2)
gs.fit(trainprueba[var_finales],trainprueba['fraude2'])


# In[252]:


gs.best_estimator_


# In[253]:


#gini train
probs = gs.best_estimator_.predict_proba(trainprueba[var_finales])
gini = 2*metrics.roc_auc_score(trainprueba['fraude2'], probs[:,1])-1
gini


# In[254]:


#gini test
probs = gs.best_estimator_.predict_proba(testprueba[var_finales])
gini = 2*metrics.roc_auc_score(testprueba['fraude2'], probs[:,1])-1
gini


# In[255]:


model = DecisionTreeClassifier(random_state = 123)
max_depth=5
min_sample_split=12
grid = {"max_depth": [max_depth-1, max_depth, max_depth+1],
        "max_features": ['auto', 'log2'],
        "min_samples_split": [min_sample_split-2, min_sample_split, min_sample_split+2],
        "min_samples_leaf": [5, 6, 7],
        "criterion": ["gini", 'entropy'],
        'class_weight': ['balanced',None],
        'max_features': [0.6,0.8,1]
        }            
gs = GridSearchCV(model, param_grid = grid, cv = kfold, n_jobs=-1, return_train_score=True, scoring = 'roc_auc',verbose=2)
gs.fit(trainprueba[var_finales],trainprueba['fraude2'])


# In[256]:


gs.best_estimator_


# In[257]:


#gini train
probs = gs.best_estimator_.predict_proba(trainprueba[var_finales])
gini = 2*metrics.roc_auc_score(trainprueba['fraude2'], probs[:,1])-1
gini


# In[258]:


#gini test
probs = gs.best_estimator_.predict_proba(testprueba[var_finales])
gini = 2*metrics.roc_auc_score(testprueba['fraude2'], probs[:,1])-1
gini

